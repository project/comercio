<?php
namespace Drupal\asir\Controller;

use Drupal;
use Drupal\node\Entity\Node;
use Drupal\Core\Controller\ControllerBase;
// Inclúense as clases dependentes a maiores para BD e xerar urls
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use \Drupal\Core\Link;

class asirController extends ControllerBase
{
    public function page()
    {
        // Página de inicio, usando hook_theme:
        return [
'#theme' => 'inicio',
'#titulo' => $this->t('Contenido del módulo'),
'#descripcion' => $this->t('Página de inicio para gestionar sus clientes.'),
'#imagen' => $this->t('../drupal/modules/asir/public/images/logo.jpg')
];
    }

    public function getClientes()
    {
        // Obter os datos da base de datos
        $consulta = \Drupal::database()->select('cliente', 'cli');
        $consulta->fields('cli', ['id', 'nombre', 'telefono', 'email', 'genero', 'cuenta']);
        $resultado = $consulta->execute();
        //Guardamos la tabla cliente en un array
        while ($clientes[]= $resultado->fetchAssoc()) {
        }
        // Enviamos os datos a unha vista, usando hook_theme:
        return [
'#theme' => 'clientes',
'#titulo' => $this->t('Listado de clientes'),
'#descripcion' => $this->t('Clientes disponibles'),
'#clientes' => $clientes,
];
    }

    public function descargarPDF()
    {
        // Obter os datos da base de datos
        $consulta = \Drupal::database()->select('cliente', 'cli');
        $consulta->fields('cli', ['id', 'nombre', 'telefono', 'email', 'genero', 'cuenta']);
        $resultado = $consulta->execute();
        //HTML para imprimir en el pdf(tabla con todos los clientes)
        while ($clientes[]= $resultado->fetchAssoc()) {
            $html = "<h3>Clientes</h3><table border='1' bordercolor='#003078' width='100%'>";
            foreach ($clientes as $cliente) {
                $html .= "<tr><td>".$cliente['id']."</td>
                <td>".$cliente['nombre']."</td>
                <td>".$cliente['telefono']."</td>
                <td>".$cliente['email']."</td>
                <td>".$cliente['genero']."</td>
                <td>".$cliente['cuenta']."</td></tr>";
            }
            $html  .= "</table>";
        }

        $mpdf = new \Mpdf\Mpdf(['tempDir' =>  __DIR__ . '/tmp']);
        $html = utf8_encode($html);
        $mpdf->WriteHTML($html);
        $mpdf->Output('clientes.pdf', 'D');
        drupal_set_message("Descarga realizada.");
        exit;
    }

    public function crearCliente()
    {
        // Utilizamos o formulario
        $form = $this->formBuilder()->getForm('Drupal\asir\Form\ClientesForm');
        //Lo renderizamos
        \Drupal::service('renderer')->render($form);
    }
    public function amosar()
    {
        // Define a cabeceira da taboa
        $header_table = array(
'id'=> t('ID'),
'nombre' => t('Nombre'),
'telefono' => t('Teléfono'),
'email'=>t('Email'),
'genero' => t('Género'),
'cuenta' => t('Nº de cuenta'),
'qr' => t('Código QR'),
'opt' => t('Operación 1'),
'opt1' => t('Operación 2'),
);
        // Selecciona os datos da taoba
        // Esténdese a clase pagerselectextender e se limita o resultado, por exemplo a 10 filas:

        $query = \Drupal::database()->select('cliente', 'cli');
        $query->fields('cli', ['id','nombre','telefono','email','genero','cuenta','qr']);
        $paxinador = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(5);
        $resultados = $paxinador->execute()->fetchAll();
        //$resultados = $query->execute()->fetchAll();
        $rows=array();
        // Vai lendo cada fila da táboa e agregándoa ao array rows
        foreach ($resultados as $data) {
            // Crea as ligazons para eliminar e modificar
            $delete = Url::fromUserInput('/asir/form/delete/'.$data->id);
            $edit = Url::fromUserInput('/asir/form/formulario?num='.$data->id);
            $link = Url::fromUri($data->qr);
            // Garda cada fila no array rows[] para amosalo despois
            $rows[] = array(
'id' =>$data->id,
'nombre' => $data->nombre,
'telefono' => $data->telefono,
'email' => $data->email,
'genero' => $data->genero,
'cuenta' => $data->cuenta,
\Drupal::l('Generar QR', $link),
// Agrega as ligazons para eliminar e modificar a taboa
\Drupal::l('Eliminar', $delete),
\Drupal::l('Editar', $edit),
);
        }
        // Xera a taboa para amosar na vista
        $form['table'] = [
'#type' => 'table',
'#header' => $header_table,
'#options' => $output,
'#rows' => $rows,
'#empty' => t('No se encontraron usuarios'),
];
        // Paxinador
        $form['pager'] = array(
'#type' => 'pager'
);
        return $form;
    }
}
