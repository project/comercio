<?php

namespace Drupal\asir\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
// Inclúense como clases a maiores:
use Drupal\Core\Url;
use Drupal\Core\Database\Database;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Component\Utility\Html;

/**
 * Class ClientesForm.
 */
class ClientesForm extends FormBase
{


  /**
   * {@inheritdoc}
   */
    public function getFormId()
    {
        return 'clientes_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $conexion = Database::getConnection();
        $rexistro = array();
        // Se recibe un id de cliente recupera os seus datos
        if (isset($_GET['num'])) {
            $query = $conexion->select('cliente', 'cli')->condition('id', $_GET['num'])->fields('cli');
            $rexistro = $query->execute()->fetchAssoc();
        }
        $form['nombre'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nombre'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => true,
      '#weight' => '0',
      '#default_value' => (isset($rexistro['nombre']) && $_GET['num']) ? $rexistro['nombre'] : '',
    ];
        $form['telefono'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Teléfono'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => true,
      '#weight' => '0',
      '#default_value' => (isset($rexistro['telefono']) && $_GET['num']) ? $rexistro['telefono'] : '',
    ];
        $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#required' => true,
      '#weight' => '0',
      '#default_value' => (isset($rexistro['email']) && $_GET['num']) ? $rexistro['email'] : '',
    ];
        $form['correo'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enviar por correo'),
      '#weight' => '0',
    ];
        $form['genero'] = [
      '#type' => 'select',
      '#title' => $this->t('Género'),
      '#options' => ['mujer' => $this->t('Mujer'), 'hombre' => $this->t('Hombre')],
      '#size' => 1,
      '#required' => true,
      '#weight' => '0',
      '#default_value' => (isset($rexistro['genero']) && $_GET['num']) ? $rexistro['genero'] : '',
    ];
        $form['cuenta'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nº de Cuenta'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => true,
      '#weight' => '0',
      '#default_value' => (isset($rexistro['cuenta']) && $_GET['num']) ? $rexistro['cuenta'] : '',
    ];

        $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Enviar'),
      '#weight' => '0',
    ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        //validación para nombre
        $nombre = $form_state->getValue('nombre');
        if (preg_match('/[^A-Za-z]/', $nombre)) {
            $form_state->setErrorByName('nombre', $this->t('El nombre solo puede contener caracteres.'));
        }
        //validación para teléfono
        $telefono = $form_state->getValue('telefono');
        if (preg_match('/[^0-9]/', $telefono)) {
            $form_state->setErrorByName('telefono', $this->t('El teléfono solo puede contener números.'));
        }
        //validación para email
        $email = $form_state->getValue('email');
        if (preg_match('/^[A-z0-9\\._-]@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/', $email)) {
            $form_state->setErrorByName('email', $this->t('El correo debe ser del tipo: alguien@algunlugar.es'));
        }
        //validación para cuenta corriente
        $cuenta = $form_state->getValue('cuenta');
        if (preg_match('/[0-9]{4}[0-9]{4}[0-9]{2}[0-9]{10}/', $cuenta)) {
            $form_state->setErrorByName('cuenta', $this->t('El nº de cuenta no es correcto.'));
        }
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Display result.
        foreach ($form_state->getValues() as $key => $value) {
            drupal_set_message($key . ': ' . $value);
        }
        $field = $form_state->getValues();
        $nombre = $field['nombre'];
        $telefono = $field['telefono'];
        $email = $field['email'];
        $genero = $field['genero'];
        $cuenta = $field['cuenta'];
        //Array para pasar los valores por url
        $field = array(
'nombre' => $nombre,
'telefono' => $telefono,
'email' => $email,
'genero' => $genero,
'cuenta' => $cuenta,
);
        $url ="https://api.qrserver.com/v1/create-qr-code/?size=200x200&data=".implode(", ", $field);

        if (isset($_GET['num'])) {
            // UPDATE

            $field = array(
'nombre' => $nombre,
'telefono' => $telefono,
'email' => $email,
'genero' => $genero,
'cuenta' => $cuenta,
'qr' => $url,
);
            $query = \Drupal::database();
            $query->update('cliente')->fields($field)->condition('id', $_GET['num'])->execute();
            drupal_set_message("Actualizado correctamente.");
            $form_state->setRedirect('asir.amosar');
        } else {
            // INSERT

            $field = array(
'nombre' => $nombre,
'telefono' => $telefono,
'email' => $email,
'genero' => $genero,
'cuenta' => $cuenta,
'qr' => $url,
);
            $query = \Drupal::database();
            $query ->insert('cliente')->fields($field)->execute();
            drupal_set_message("Añadido correctamente.");
            $response = new RedirectResponse(\Drupal::url('asir.amosar'));
            $response->send();
        }
        //Enviar por correo
        if (isset($_POST['correo'])) {
            function asir_faq()
            {
                $account = $user->email;
                $param = array();
                // Envía o email. Drupal redirixe ao hook_mail() nesta sentenza:
                $mail_sent = \Drupal::service('plugin.manager.mail')->mail(
                    'asir',
                    'notify_asker',
                    $email,
                    $account->getPreferredLangcode(),
                    $params,
                    null,
                    true
);
                // Comproba se foi correcto o envío
                if ($mail_sent) {
                    print "Correo enviado correctamente.";
                } else {
                    print "Hubo un error al enviar el email.";
                }
            }
            /**
            * Implements hook_mail().
            * Esta función completa o email
            */
            function asir_mail($key, &$message, $params)
            {
                if (($key == 'notify_asker')) {
                    $message['from'] = 'asirdrupalmodules@gmail.com';
                    $message['body'] = "Le enviamos el enlace de su compra: ".$url;
                    $message['subject'] = "Código QR";
                }
            }
        }
    }
}
