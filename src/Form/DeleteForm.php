<?php

namespace Drupal\asir\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
// Engádense as clases para as operacións a realizar
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Render\Element;

/**
 * Class DeleteForm.
 */
class DeleteForm extends ConfirmFormBase
{
    public $cid;
    private $id;
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'delete_form';
    }
    public function getQuestion()
    {
        return t('¿Seguro que deseas eliminar el cliente con id: %cid?', array('%cid' => $this->id));
    }
    public function getCancelUrl()
    {
        return new Url('asir.amosar');
    }
    public function getDescription()
    {
        return t('Solo lo deberías eliminar si estás seguro/a.');
    }
    public function getConfirmText()
    {
        return t('Eliminar');
    }
    public function getCancelText()
    {
        return t('Cancelar');
    }
    public function buildForm(array $form, FormStateInterface $form_state, $cid = null)
    {
        $this->id = $cid;
        return parent::buildForm($form, $form_state);
    }
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $query = \Drupal::database();
        $query->delete('cliente')->condition('id', $this->id)->execute();
        drupal_set_message("Eliminado correctamente.");
        $form_state->setRedirect('asir.amosar');
    }
}
